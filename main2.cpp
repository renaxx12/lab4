#include <iostream> 
#include "grafo.cpp" 

using namespace std; 

int main() { 
    GrafoTipo<int> x(10); 

    cout << "Vació? " << x.EstaVacio() << " - " << x.Tamanio() << endl;

    
    x.AgregarVertice(10);
    x.AgregarVertice(20);
    cout << "\nVació? " << x.EstaVacio() << " - " << x.Tamanio() << endl;
    x.AgregarArista(10, 20, 100);
    cout << "Peso: " << x.Peso(10, 20) << endl;
    x.AgregarVertice(30);
    cout << "\nVació? " << x.EstaVacio() << " - " << x.Tamanio() << endl;
    x.AgregarArista(10, 30, 200);
    cout << "Peso: " << x.Peso(10, 30) << endl;
    
} 