const int NULL_EDGE = 0;
template<class T>
class GrafoTipo {
  public:
    GrafoTipo(int);
    ~GrafoTipo();
    void AgregarVertice(T);
    void AgregarArista(T, T, int);
    int Peso(T, T);
    int Indice(T*, T);
    bool EstaVacio();
    bool EstaLleno(); 
    int Tamanio();

 private:
    int numVertices;
    int maxVertices;

    int* vertices;
    int **aristas;

    bool* marks;
}; 