#include "grafo.h"

template<class T>
GrafoTipo<T>::GrafoTipo(int maxV){
   numVertices = 0;
   maxVertices = maxV;
   vertices = new int[maxV];
   aristas = new int*[maxV];
 
   for(int i = 0; i < maxV; i++)
     aristas[i] = new int[maxV];
 
   marks = new bool[maxV];
}
template<class T>
GrafoTipo<T>::~GrafoTipo(){
   delete [] vertices;
 
   for(int i = 0; i < maxVertices; i++)
      delete [] aristas[i];
 
   delete [] aristas;
   delete [] marks;
} 
template<class T>
void GrafoTipo<T>::AgregarVertice(T vertice){
   vertices[numVertices] = vertice;

   for(int indice = 0; indice < numVertices; indice++) {
     aristas[numVertices][indice] = NULL_EDGE;
     aristas[indice][numVertices] = NULL_EDGE;
   }
   
   numVertices++;
}
template<class T>
void GrafoTipo<T>::AgregarArista(T desdeVertice, T haciaVertice, int peso){
   int fila, col;

   fila = Indice(vertices, desdeVertice);
   col = Indice(vertices, haciaVertice);
   aristas[fila][col] = peso;
} 
template<class T>
int GrafoTipo<T>::Peso(T desdeVertice, T haciaVertice){
   int fila, col;

   fila = Indice(vertices, desdeVertice);
   col = Indice(vertices, haciaVertice);
   
   return aristas[fila][col];
} 
template<class T>
int GrafoTipo<T>::Indice(T *vertices, T vertice){
  for(int i = 0; i < numVertices; i++){
      if (vertices[i] == vertice)
          return i;
  }

  return -1;
}
template<class T>
bool GrafoTipo<T>::EstaLleno(){
  if (numVertices == maxVertices)
      return true;
  
  return false;
}
template<class T>
bool GrafoTipo<T>::EstaVacio(){
  if (numVertices == 0)
      return true;
  
  return false;
}
template<class T>
int GrafoTipo<T>::Tamanio(){
  return numVertices;
}